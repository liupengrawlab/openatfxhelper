package de.rechner.openatfx.io;

import de.rechner.openatfx.util.BufferedRandomAccessFile;
import de.rechner.openatfx.util.FileUtil;
import de.rechner.openatfx.util.ModelCache;
import de.rechner.openatfx.util.ODSHelper;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.asam.ods.AIDName;
import org.asam.ods.AIDNameValueSeqUnitId;
import org.asam.ods.AoException;
import org.asam.ods.AoSession;
import org.asam.ods.ApplAttr;
import org.asam.ods.ApplElem;
import org.asam.ods.ApplElemAccess;
import org.asam.ods.ApplRel;
import org.asam.ods.ApplicationElement;
import org.asam.ods.ApplicationRelation;
import org.asam.ods.ApplicationStructure;
import org.asam.ods.Blob;
import org.asam.ods.DataType;
import org.asam.ods.ElemId;
import org.asam.ods.EnumerationDefinition;
import org.asam.ods.ErrorCode;
import org.asam.ods.InstanceElement;
import org.asam.ods.NameValueUnit;
import org.asam.ods.SetType;
import org.asam.ods.SeverityFlag;
import org.asam.ods.TS_Union;
import org.asam.ods.TS_Value;
import org.asam.ods.TS_ValueSeq;
import org.asam.ods.T_COMPLEX;
import org.asam.ods.T_DCOMPLEX;
import org.asam.ods.T_ExternalReference;
import org.asam.ods.T_LONGLONG;
import org.codehaus.stax2.typed.TypedXMLStreamException;

class AtfxInstanceReader {
  private static final Log LOG = LogFactory.getLog(AtfxInstanceReader.class);

  private static volatile AtfxInstanceReader instance;

  public void parseInstanceElements(AoSession aoSession, Map<String, String> files,
      XMLStreamReader reader) throws XMLStreamException, AoException {
    ModelCache modelCache = new ModelCache(aoSession.getApplicationStructureValue(),
        aoSession.getEnumerationAttributes(), aoSession.getEnumerationStructure());
    Map<ElemId, Map<String, T_LONGLONG[]>> relMap = new HashMap<>();
    long start = System.currentTimeMillis();
    File flagsFile = getFlagsTmpFile(aoSession);
    if (flagsFile.isFile() && flagsFile.exists() && flagsFile.length() > 0L
        && flagsFile.canWrite()) {
      if (!flagsFile.delete())
        throw new AoException(ErrorCode.AO_UNKNOWN_ERROR, SeverityFlag.ERROR, 0,
            "Unable to delete file '" + flagsFile + "'");
      LOG.info("Deleted existing flag file: " + flagsFile.getName());
    }
    reader.next();
    while (!reader.isEndElement() || !reader.getLocalName().equals("instance_data")) {
      if (reader.isStartElement())
        relMap.putAll(parseInstanceElement(aoSession, files, modelCache, reader));
      reader.next();
    }
    LOG.info("Parsed instances in " + (System.currentTimeMillis() - start) + " ms");
    start = System.currentTimeMillis();
    ApplElemAccess applElemAccess = aoSession.getApplElemAccess();
    for (Map.Entry<ElemId, Map<String, T_LONGLONG[]>> entry : relMap.entrySet()) {
      ElemId elemId = entry.getKey();
      for (Map.Entry<String, T_LONGLONG[]> relInstEntry : (Iterable<Map.Entry<String, T_LONGLONG[]>>) ((Map) entry
          .getValue()).entrySet()) {
        String relName = relInstEntry.getKey();
        T_LONGLONG[] relIids = relInstEntry.getValue();
        applElemAccess.setRelInst(elemId, relName, relIids, SetType.APPEND);
      }
    }
    LOG.info("Set relations in " + (System.currentTimeMillis() - start) + " ms");
  }

  private Map<ElemId, Map<String, T_LONGLONG[]>> parseInstanceElement(AoSession aoSession,
      Map<String, String> files, ModelCache modelCache, XMLStreamReader reader)
      throws XMLStreamException, AoException {
    String aeName = reader.getLocalName();
    ApplElem applElem = modelCache.getApplElem(aeName);
    if (applElem == null)
      throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0,
          "ApplicationElement '" + aeName + "' not found");
    Long aid = Long.valueOf(ODSHelper.asJLong(applElem.aid));
    boolean ismeaq = applElem.beName.equals("AoMeasurementQuantity");
    List<AIDNameValueSeqUnitId> applAttrValues = new ArrayList<>();
    List<NameValueUnit> instAttrValues = new ArrayList<>();
    Map<String, T_LONGLONG[]> instRelMap = new HashMap<>();
    InstanceElement ieExternalComponent = null;
    String currentTagName = null;
    while (!reader.isEndElement() || !reader.getLocalName().equals(aeName)
        || currentTagName != null) {
      if (reader.isEndElement() && currentTagName != null)
        currentTagName = null;
      reader.next();
      if (reader.isStartElement())
        currentTagName = reader.getLocalName();
      if (reader.isStartElement() && modelCache.isLocalColumnValuesAttr(aeName, currentTagName)) {
        reader.nextTag();
        if (reader.isStartElement() && reader.getLocalName().equals("component")) {
          if (ieExternalComponent == null)
            ieExternalComponent = createExtCompIe(aoSession);
          parseLocalColumnValuesComponent(ieExternalComponent, files, modelCache, reader);
          continue;
        }
        if (reader.isStartElement()) {
          TS_Value value = parseLocalColumnValues(modelCache, reader);
          AIDNameValueSeqUnitId valuesAttrValue = new AIDNameValueSeqUnitId();
          valuesAttrValue.unitId = ODSHelper.asODSLongLong(0L);
          valuesAttrValue.attr = new AIDName();
          valuesAttrValue.attr.aid = applElem.aid;
          valuesAttrValue.attr.aaName = modelCache.getLcValuesAaName();
          valuesAttrValue.values = ODSHelper.tsValue2tsValueSeq(value);
          applAttrValues.add(valuesAttrValue);
        }
        continue;
      }
      if (reader.isStartElement() && reader.getLocalName().equals("instance_attributes")) {
        instAttrValues = parseInstanceAttributes(reader);
        continue;
      }
      if (reader.isStartElement() && modelCache.isLocalColumnFlagsAttr(aeName, currentTagName)) {
        try {
          ApplAttr applAttr = modelCache.getApplAttr(aid, currentTagName);
          AIDNameValueSeqUnitId applAttrValue = new AIDNameValueSeqUnitId();
          applAttrValue.unitId = ODSHelper.asODSLongLong(0L);
          applAttrValue.attr = new AIDName();
          applAttrValue.attr.aid = applElem.aid;
          applAttrValue.attr.aaName = currentTagName;
          applAttrValue.values = new TS_ValueSeq();
          applAttrValue.values = ODSHelper.tsValue2tsValueSeq(parseAttributeContent(aoSession, aid,
              currentTagName, applAttr.dType, modelCache, reader));
          applAttrValues.add(applAttrValue);
        } catch (TypedXMLStreamException e) {
          if (ieExternalComponent == null)
            ieExternalComponent = createExtCompIe(aoSession);
          parseLocalColumnFlagsComponent(ieExternalComponent, files, modelCache, reader);
        }
        continue;
      }
      if (reader.isStartElement() && modelCache.getApplAttr(aid, currentTagName) != null) {
        ApplAttr applAttr = modelCache.getApplAttr(aid, currentTagName);
        AIDNameValueSeqUnitId applAttrValue = new AIDNameValueSeqUnitId();
        applAttrValue.unitId = ODSHelper.asODSLongLong(0L);
        applAttrValue.attr = new AIDName();
        applAttrValue.attr.aid = applElem.aid;
        applAttrValue.attr.aaName = currentTagName;
        applAttrValue.values = ODSHelper.tsValue2tsValueSeq(parseAttributeContent(aoSession, aid,
            currentTagName, applAttr.dType, modelCache, reader));
        applAttrValues.add(applAttrValue);
        continue;
      }
      if (reader.isStartElement() && modelCache.getApplRel(aid, currentTagName) != null) {
        ApplRel applRel = modelCache.getApplRel(aid, currentTagName);
        short relMax = applRel.arRelationRange.max;
        short invMax = applRel.invRelationRange.max;
        if (relMax == -1 || (relMax == 1 && invMax == 1)
            || applRel.brName.equals("measurement_quantity")
            || (ismeaq && applRel.brName.equals("unit"))) {
          String textContent = reader.getElementText();
          if (textContent.length() > 0) {
            T_LONGLONG[] relInstIids = AtfxParseUtil.parseLongLongSeq(textContent);
            instRelMap.put(applRel.arName, relInstIids);
          }
        }
        continue;
      }
      if (reader.isStartElement() && reader.getLocalName().equals("ACLA"))
        continue;
      if (reader.isStartElement() && reader.getLocalName().equals("ACLI"))
        continue;
      if (reader.isStartElement())
        LOG.warn("Unsupported XML tag name: " + reader.getLocalName());
    }
    if (applAttrValues.isEmpty())
      return Collections.emptyMap();
    ApplElemAccess aea = aoSession.getApplElemAccess();
    ElemId elemId = aea.insertInstances(
        (AIDNameValueSeqUnitId[]) applAttrValues.toArray(new AIDNameValueSeqUnitId[0]))[0];
    if (!instAttrValues.isEmpty()) {
      InstanceElement ie =
          aoSession.getApplicationStructure().getInstancesById(new ElemId[] {elemId})[0];
      for (NameValueUnit nvu : instAttrValues)
        ie.addInstanceAttribute(nvu);
    }
    if (ieExternalComponent != null) {
      ApplicationStructure as = aoSession.getApplicationStructure();
      ApplicationElement aeLocalColumn = as.getElementById(elemId.aid);
      ApplicationRelation rel = aeLocalColumn.getRelationsByBaseName("external_component")[0];
      InstanceElement ieLocalColumn = as.getElementById(elemId.aid).getInstanceById(elemId.iid);
      ieLocalColumn.createRelation(rel, ieExternalComponent);
      String attrSeqRep = aeLocalColumn.getAttributeByBaseName("sequence_representation").getName();
      int seqRepOrig = ODSHelper.getEnumVal(ieLocalColumn.getValue(attrSeqRep));
      int seqRep = ODSHelper.seqRepComp2seqRepExtComp(seqRepOrig);
      ieLocalColumn.setValue(ODSHelper.createEnumNVU(attrSeqRep, seqRep));
    }
    Map<ElemId, Map<String, T_LONGLONG[]>> retMap = new HashMap<>();
    retMap.put(new ElemId(applElem.aid, elemId.iid), instRelMap);
    return retMap;
  }

  private InstanceElement createExtCompIe(AoSession aoSession) throws AoException {
    ApplicationStructure as = aoSession.getApplicationStructure();
    ApplicationElement[] aes = as.getElementsByBaseType("AoExternalComponent");
    if (aes.length != 1)
      throw new AoException(ErrorCode.AO_UNKNOWN_ERROR, SeverityFlag.ERROR, 0,
          "None or multiple application elements of type 'AoExternalComponent' found");
    ApplicationElement aeExtComp = aes[0];
    return aeExtComp.createInstance("ExtComp");
  }

  private void parseLocalColumnValuesComponent(InstanceElement ieExtComp, Map<String, String> files,
      ModelCache modelCache, XMLStreamReader reader) throws XMLStreamException, AoException {
    ApplicationElement aeExtComp = ieExtComp.getApplicationElement();
    ApplicationStructure as = aeExtComp.getApplicationStructure();
    EnumerationDefinition typeSpectEnum = as.getEnumerationDefinition("typespec_enum");
    long aidExtComp = ODSHelper.asJLong(aeExtComp.getId());
    String description = "";
    String fileName = "";
    int dataType = 0;
    int length = 0;
    long inioffset = 0L;
    int blockSize = 0;
    int valPerBlock = 0;
    int valOffsets = 0;
    Short bitCount = null;
    Short bitOffset = null;
    while (!reader.isEndElement() || !reader.getLocalName().equals("component")) {
      if (reader.isStartElement() && reader.getLocalName().equals("description")) {
        description = reader.getElementText();
      } else if (reader.isStartElement() && reader.getLocalName().equals("identifier")) {
        String identifier = reader.getElementText();
        fileName = files.get(identifier);
        if (fileName == null)
          throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0,
              "External component file not found for identifier '" + identifier + "'");
      } else if (reader.isStartElement() && reader.getLocalName().equals("datatype")) {
        dataType = typeSpectEnum.getItem(reader.getElementText());
      } else if (reader.isStartElement() && reader.getLocalName().equals("length")) {
        length = parseFileLength(reader.getElementText(), "length");
      } else if (reader.isStartElement() && reader.getLocalName().equals("inioffset")) {
        inioffset = parseStartOffset(reader.getElementText(), "inioffset");
      } else if (reader.isStartElement() && reader.getLocalName().equals("blocksize")) {
        blockSize = parseFileLength(reader.getElementText(), "blocksize");
      } else if (reader.isStartElement() && reader.getLocalName().equals("valperblock")) {
        valPerBlock = parseFileLength(reader.getElementText(), "valperblock");
      } else if (reader.isStartElement() && reader.getLocalName().equals("valoffsets")) {
        valOffsets = parseFileLength(reader.getElementText(), "valoffsets");
      } else if (reader.isStartElement() && reader.getLocalName().equals("bitcount")) {
        bitCount = Short.valueOf(AtfxParseUtil.parseShort(reader.getElementText()));
      } else if (reader.isStartElement() && reader.getLocalName().equals("bitoffset")) {
        bitOffset = Short.valueOf(AtfxParseUtil.parseShort(reader.getElementText()));
      }
      reader.next();
    }
    List<NameValueUnit> attrsList = new ArrayList<>();
    ApplAttr applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "filename_url");
    attrsList.add(ODSHelper.createStringNVU(applAttr.aaName, fileName));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "value_type");
    attrsList.add(ODSHelper.createEnumNVU(applAttr.aaName, dataType));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "component_length");
    attrsList.add(ODSHelper.createLongNVU(applAttr.aaName, length));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "start_offset");
    if (applAttr.dType == DataType.DT_LONG) {
      attrsList.add(ODSHelper.createLongNVU(applAttr.aaName, (int) inioffset));
    } else {
      attrsList.add(ODSHelper.createLongLongNVU(applAttr.aaName, inioffset));
    }
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "block_size");
    attrsList.add(ODSHelper.createLongNVU(applAttr.aaName, blockSize));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "valuesperblock");
    attrsList.add(ODSHelper.createLongNVU(applAttr.aaName, valPerBlock));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "value_offset");
    attrsList.add(ODSHelper.createLongNVU(applAttr.aaName, valOffsets));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "description");
    if (applAttr != null && applAttr.aaName.length() > 0)
      attrsList.add(ODSHelper.createStringNVU(applAttr.aaName, description));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "ordinal_number");
    if (applAttr != null && applAttr.aaName.length() > 0)
      attrsList.add(ODSHelper.createLongNVU(applAttr.aaName, 1));
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "ao_bit_count");
    if (bitCount != null) {
      if (applAttr == null)
        throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0,
            "No application attribute of type 'ao_bit_count' found!");
      attrsList.add(ODSHelper.createShortNVU(applAttr.aaName, bitCount.shortValue()));
    }
    applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "ao_bit_offset");
    if (bitOffset != null) {
      if (applAttr == null)
        throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0,
            "No application attribute of type 'ao_bit_offset' found!");
      attrsList.add(ODSHelper.createShortNVU(applAttr.aaName, bitOffset.shortValue()));
    }
    ieExtComp.setValueSeq(attrsList.<NameValueUnit>toArray(new NameValueUnit[0]));
  }

  private void parseLocalColumnFlagsComponent(InstanceElement ieExtComp, Map<String, String> files,
      ModelCache modelCache, XMLStreamReader reader) throws XMLStreamException, AoException {
    BufferedRandomAccessFile bufferedRandomAccessFile = null;
    ApplicationElement aeExtComp = ieExtComp.getApplicationElement();
    ApplicationStructure as = aeExtComp.getApplicationStructure();
    EnumerationDefinition typeSpectEnum = as.getEnumerationDefinition("typespec_enum");
    long aidExtComp = ODSHelper.asJLong(aeExtComp.getId());
    String fileName = "";
    int dataType = 0;
    int length = 0;
    long inioffset = 0L;
    int blockSize = 0;
    int valPerBlock = 0;
    int valOffsets = 0;
    while (!reader.isEndElement() || !reader.getLocalName().equals("component")) {
      if (reader.isStartElement() && reader.getLocalName().equals("identifier")) {
        String identifier = reader.getElementText();
        fileName = files.get(identifier);
        if (fileName == null)
          throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0,
              "External component file not found for identifier '" + identifier + "'");
      } else if (reader.isStartElement() && reader.getLocalName().equals("datatype")) {
        dataType = typeSpectEnum.getItem(reader.getElementText());
        if (dataType != 2)
          throw new AoException(ErrorCode.AO_NOT_IMPLEMENTED, SeverityFlag.ERROR, 0,
              "Unsupported 'dataType' for flags component file: " + dataType);
      } else if (reader.isStartElement() && reader.getLocalName().equals("length")) {
        length = parseFileLength(reader.getElementText(), "length");
      } else if (reader.isStartElement() && reader.getLocalName().equals("inioffset")) {
        inioffset = parseStartOffset(reader.getElementText(), "inioffset");
      } else if (reader.isStartElement() && reader.getLocalName().equals("blocksize")) {
        blockSize = parseFileLength(reader.getElementText(), "blocksize");
      } else if (reader.isStartElement() && reader.getLocalName().equals("valperblock")) {
        valPerBlock = parseFileLength(reader.getElementText(), "valperblock");
      } else if (reader.isStartElement() && reader.getLocalName().equals("valoffsets")) {
        valOffsets = parseFileLength(reader.getElementText(), "valoffsets");
      }
      reader.next();
    }
    if (valPerBlock == 0)
      valPerBlock = length;
    if (blockSize == 0)
      blockSize = length * 2;
    long start = System.currentTimeMillis();
    AoSession aoSession = ieExtComp.getApplicationElement().getApplicationStructure().getSession();
    File fileRoot = new File((aoSession.getContextByName("FILE_ROOT")).value.u.stringVal());
    File componentFile = new File(fileRoot, fileName);
    File flagsFile = getFlagsTmpFile(aoSession);
    RandomAccessFile raf = null;
    OutputStream fos = null;
    try {
      bufferedRandomAccessFile = new BufferedRandomAccessFile(componentFile, "r", 32768);
      bufferedRandomAccessFile.seek(inioffset);
      fos = new BufferedOutputStream(new FileOutputStream(flagsFile, true));
      long startOffset = flagsFile.length();
      ByteBuffer sourceMbb = ByteBuffer.allocate(blockSize);
      sourceMbb.order(ByteOrder.LITTLE_ENDIAN);
      int i;
      for (i = 0; i < length; i += valPerBlock) {
        byte[] buffer = new byte[blockSize];
        bufferedRandomAccessFile.read(buffer, 0, buffer.length);
        ((Buffer) Buffer.class.cast(sourceMbb)).clear();
        sourceMbb.put(buffer);
        sourceMbb.position(valOffsets);
        for (int j = 0; j < valPerBlock; j++) {
          byte[] b = new byte[2];
          sourceMbb.get(b);
          fos.write(b);
        }
      }
      ApplAttr applAttr =
          modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "flags_filename_url");
      ieExtComp.setValue(ODSHelper.createStringNVU(applAttr.aaName, flagsFile.getName()));
      applAttr = modelCache.getApplAttrByBaseName(Long.valueOf(aidExtComp), "flags_start_offset");
      if (applAttr.dType == DataType.DT_LONG) {
        ieExtComp.setValue(ODSHelper.createLongNVU(applAttr.aaName, (int) startOffset));
      } else {
        ieExtComp.setValue(ODSHelper.createLongLongNVU(applAttr.aaName, startOffset));
      }
      LOG.info("Copied " + length + " flags from component file '" + fileName
          + "' to external component '" + flagsFile.getName() + "' in "
          + (System.currentTimeMillis() - start) + "ms");
    } catch (IOException e) {
      LOG.error(e.getMessage(), e);
      throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0, e.getMessage());
    } finally {
      if (bufferedRandomAccessFile != null) {
        try {
          bufferedRandomAccessFile.close();
        } catch (IOException ioe) {
          LOG.error(ioe.getMessage(), ioe);
        }
        bufferedRandomAccessFile = null;
      }
      if (fos != null) {
        try {
          fos.close();
        } catch (IOException ioe) {
          LOG.error(ioe.getMessage(), ioe);
        }
        fos = null;
      }
    }
  }

  private File getFlagsTmpFile(AoSession aoSession) throws AoException {
    File fileRoot = new File((aoSession.getContextByName("FILE_ROOT")).value.u.stringVal());
    File atfxFile = new File((aoSession.getContextByName("FILENAME")).value.u.stringVal());
    File flagsFile =
        new File(fileRoot, FileUtil.stripExtension(atfxFile.getName()) + "_flags_extract.btf");
    return flagsFile;
  }

  private TS_Value parseLocalColumnValues(ModelCache modelCache, XMLStreamReader reader)
      throws XMLStreamException, AoException {
    TS_Value value = new TS_Value();
    value.flag = 15;
    value.u = new TS_Union();
    while (!reader.isEndElement()
        || !reader.getLocalName().equals(modelCache.getLcValuesAaName())) {
      if (!reader.isStartElement() || !reader.getLocalName().equals("A_BLOB"))
        if (reader.isStartElement() && reader.getLocalName().equals("A_BOOLEAN")) {
          value.u.booleanSeq(AtfxParseUtil.parseBooleanSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_COMPLEX32")) {
          value.u.complexSeq(AtfxParseUtil.parseComplexSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_COMPLEX64")) {
          value.u.dcomplexSeq(AtfxParseUtil.parseDComplexSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_EXTERNALREFERENCE")) {
          value.u.extRefSeq(parseExtRefs("A_EXTERNALREFERENCE", reader));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_BYTEFIELD")) {
          value.u.bytestrSeq(parseBytestrSeq("A_BYTEFIELD", reader));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_INT8")) {
          value.u.byteSeq(AtfxParseUtil.parseByteSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_INT16")) {
          value.u.shortSeq(AtfxParseUtil.parseShortSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_INT32")) {
          value.u.longSeq(AtfxParseUtil.parseLongSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_INT64")) {
          value.u.longlongSeq(AtfxParseUtil.parseLongLongSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_FLOAT32")) {
          value.u.floatSeq(AtfxParseUtil.parseFloatSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_FLOAT64")) {
          value.u.doubleSeq(AtfxParseUtil.parseDoubleSeq(reader.getElementText()));
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_TIMESTRING")) {
          String input = reader.getElementText().trim();
          String[] dateSeq = new String[0];
          if (input.length() > 0)
            dateSeq = input.split("\\s+");
          value.u.dateSeq(dateSeq);
        } else if (reader.isStartElement() && reader.getLocalName().equals("A_UTF8STRING")) {
          value.u.stringSeq(parseStringSeq("A_UTF8STRING", reader));
        } else if (reader.isStartElement()) {
          throw new AoException(ErrorCode.AO_INVALID_DATATYPE, SeverityFlag.ERROR, 0,
              "Unsupported local column 'values' datatype: " + reader.getLocalName());
        }
      reader.nextTag();
    }
    return value;
  }

  private List<NameValueUnit> parseInstanceAttributes(XMLStreamReader reader)
      throws XMLStreamException, AoException {
    List<NameValueUnit> instAttrs = new ArrayList<>();
    while (!reader.isEndElement() || !reader.getLocalName().equals("instance_attributes")) {
      reader.next();
      if (reader.isStartElement()) {
        NameValueUnit nvu = new NameValueUnit();
        nvu.unit = "";
        nvu.valName = reader.getAttributeValue(null, "name");
        nvu.value = new TS_Value();
        nvu.value.u = new TS_Union();
        String textContent = reader.getElementText();
        if (reader.getLocalName().equals("inst_attr_asciistring")) {
          nvu.value.u.stringVal(textContent);
          nvu.value.flag = (short) ((textContent == null || textContent.length() < 1) ? 0 : 15);
        } else if (reader.getLocalName().equals("inst_attr_float32")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.floatVal(AtfxParseUtil.parseFloat(textContent));
            nvu.value.flag = 15;
          } else {
            nvu.value.u.floatVal(0.0F);
            nvu.value.flag = 0;
          }
        } else if (reader.getLocalName().equals("inst_attr_float64")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.doubleVal(AtfxParseUtil.parseDouble(textContent));
            nvu.value.flag = 15;
          } else {
            nvu.value.u.doubleVal(0.0D);
            nvu.value.flag = 0;
          }
        } else if (reader.getLocalName().equals("inst_attr_int8")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.byteVal(AtfxParseUtil.parseByte(textContent));
            nvu.value.flag = 15;
          } else {
            nvu.value.u.byteVal((byte) 0);
            nvu.value.flag = 0;
          }
        } else if (reader.getLocalName().equals("inst_attr_uint8")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.shortVal(AtfxParseUtil.parseShort(textContent));
            nvu.value.flag = 15;
          } else {
            nvu.value.u.byteVal((byte) 0);
            nvu.value.flag = 0;
          }
        } else if (reader.getLocalName().equals("inst_attr_int16")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.shortVal(AtfxParseUtil.parseShort(textContent));
            nvu.value.flag = 15;
          } else {
            nvu.value.u.shortVal((short) 0);
            nvu.value.flag = 0;
          }
        } else if (reader.getLocalName().equals("inst_attr_int32")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.longVal(AtfxParseUtil.parseLong(textContent));
            nvu.value.flag = 15;
          } else {
            nvu.value.u.longVal(0);
            nvu.value.flag = 0;
          }
        } else if (reader.getLocalName().equals("inst_attr_int64")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.longlongVal(AtfxParseUtil.parseLongLong(textContent));
            nvu.value.flag = 15;
          } else {
            nvu.value.u.longlongVal(ODSHelper.asODSLongLong(0L));
            nvu.value.flag = 0;
          }
        } else if (reader.getLocalName().equals("inst_attr_time")) {
          if (textContent.trim().length() > 0) {
            nvu.value.u.dateVal(textContent.trim());
            nvu.value.flag = 15;
          } else {
            nvu.value.u.dateVal("");
            nvu.value.flag = 0;
          }
        }
        instAttrs.add(nvu);
      }
    }
    return instAttrs;
  }

  private TS_Value parseAttributeContent(AoSession aoSession, Long aid, String aaName,
      DataType dataType, ModelCache modelCache, XMLStreamReader reader)
      throws XMLStreamException, AoException {
    TS_Value tsValue = ODSHelper.createEmptyTS_Value(dataType);
    if (dataType == DataType.DT_BLOB) {
      Blob blob = parseBlob(aoSession, aaName, reader);
      tsValue.u.blobVal(blob);
      tsValue.flag = (short) ((blob.getHeader().length() < 1 && blob.getLength() < 1) ? 0 : 15);
    } else if (dataType == DataType.DT_BOOLEAN) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        tsValue.u.booleanVal(AtfxParseUtil.parseBoolean(txt));
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_BYTE) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        tsValue.u.byteVal(AtfxParseUtil.parseByte(txt));
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_BYTESTR) {
      byte[] bytes = parseBytestr(aaName, reader);
      if (bytes.length > 0) {
        tsValue.u.bytestrVal(bytes);
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_COMPLEX) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        tsValue.u.complexVal(AtfxParseUtil.parseComplex(txt));
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_DATE) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        tsValue.u.dateVal(txt);
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_DCOMPLEX) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        tsValue.u.dcomplexVal(AtfxParseUtil.parseDComplex(txt));
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_DOUBLE) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        tsValue.u.doubleVal(AtfxParseUtil.parseDouble(txt));
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_ENUM) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        String enumName = modelCache.getEnumName(aid, aaName);
        int enumItem = modelCache.getEnumIndex(enumName, txt);
        tsValue.u.enumVal(enumItem);
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_EXTERNALREFERENCE) {
      T_ExternalReference[] extRefs = parseExtRefs(aaName, reader);
      if (extRefs.length > 1)
        throw new AoException(ErrorCode.AO_INVALID_LENGTH, SeverityFlag.ERROR, 0,
            "Multiple references for datatype DT_EXTERNALREFERENCE found");
      if (extRefs.length == 1) {
        tsValue.u.extRefVal(extRefs[0]);
        tsValue.flag = 15;
      }
    } else if (dataType == DataType.DT_FLOAT) {
      String txt = reader.getElementText().trim();
      if (txt.length() > 0) {
        tsValue.u.floatVal(AtfxParseUtil.parseFloat(txt));
        tsValue.flag = 15;
      }
    } else {
      if (dataType == DataType.DT_ID)
        throw new AoException(ErrorCode.AO_IMPLEMENTATION_PROBLEM, SeverityFlag.ERROR, 0,
            "DataType 'DT_ID' not supported for application attribute");
      if (dataType == DataType.DT_LONG) {
        String txt = reader.getElementText().trim();
        if (txt.length() > 0) {
          tsValue.u.longVal(AtfxParseUtil.parseLong(txt));
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DT_LONGLONG) {
        String txt = reader.getElementText().trim();
        if (txt.length() > 0) {
          tsValue.u.longlongVal(AtfxParseUtil.parseLongLong(txt));
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DT_SHORT) {
        String txt = reader.getElementText().trim();
        if (txt.length() > 0) {
          tsValue.u.shortVal(AtfxParseUtil.parseShort(txt));
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DT_STRING) {
        String txt = reader.getElementText();
        if (txt.length() > 0) {
          tsValue.u.stringVal(txt);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_BOOLEAN) {
        boolean[] seq = AtfxParseUtil.parseBooleanSeq(reader.getElementText());
        if (seq.length > 0) {
          tsValue.u.booleanSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_BYTE) {
        byte[] seq = AtfxParseUtil.parseByteSeq(reader.getElementText());
        if (seq.length > 0) {
          tsValue.u.byteSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_BYTESTR) {
        byte[][] seq = parseBytestrSeq(aaName, reader);
        if (seq.length > 0) {
          tsValue.u.bytestrSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_COMPLEX) {
        T_COMPLEX[] seq = AtfxParseUtil.parseComplexSeq(reader.getElementText());
        if (seq.length > 0) {
          tsValue.u.complexSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_DATE) {
        String[] seq = AtfxParseUtil.parseDateSeq(reader.getElementText());
        if (seq.length > 0) {
          tsValue.u.dateSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_DCOMPLEX) {
        T_DCOMPLEX[] seq = AtfxParseUtil.parseDComplexSeq(reader.getElementText());
        if (seq.length > 0) {
          tsValue.u.dcomplexSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_DOUBLE) {
        double[] seq = AtfxParseUtil.parseDoubleSeq(reader.getElementText());
        if (seq.length > 0) {
          tsValue.u.doubleSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_ENUM) {
        String[] seq = parseStringSeq(aaName, reader);
        if (seq.length > 0) {
          String enumName = modelCache.getEnumName(aid, aaName);
          int[] enumItems = new int[seq.length];
          for (int i = 0; i < enumItems.length; i++)
            enumItems[i] = modelCache.getEnumIndex(enumName, seq[i]);
          tsValue.u.enumSeq(enumItems);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_EXTERNALREFERENCE) {
        T_ExternalReference[] seq = parseExtRefs(aaName, reader);
        if (seq.length > 0) {
          tsValue.u.extRefSeq(seq);
          tsValue.flag = 15;
        }
      } else if (dataType == DataType.DS_FLOAT) {
        float[] seq = AtfxParseUtil.parseFloatSeq(reader.getElementText());
        if (seq.length > 0) {
          tsValue.u.floatSeq(seq);
          tsValue.flag = 15;
        }
      } else {
        if (dataType == DataType.DS_ID)
          throw new AoException(ErrorCode.AO_IMPLEMENTATION_PROBLEM, SeverityFlag.ERROR, 0,
              "DataType 'DS_ID' not supported for application attribute");
        if (dataType == DataType.DS_LONG) {
          int[] seq = AtfxParseUtil.parseLongSeq(reader.getElementText());
          if (seq.length > 0) {
            tsValue.u.longSeq(seq);
            tsValue.flag = 15;
          }
        } else if (dataType == DataType.DS_LONGLONG) {
          T_LONGLONG[] seq = AtfxParseUtil.parseLongLongSeq(reader.getElementText());
          if (seq.length > 0) {
            tsValue.u.longlongSeq(seq);
            tsValue.flag = 15;
          }
        } else if (dataType == DataType.DS_SHORT) {
          short[] seq = AtfxParseUtil.parseShortSeq(reader.getElementText());
          if (seq.length > 0) {
            tsValue.u.shortSeq(seq);
            tsValue.flag = 15;
          }
        } else if (dataType == DataType.DS_STRING) {
          String[] seq = parseStringSeq(aaName, reader);
          if (seq.length > 0) {
            tsValue.u.stringSeq(seq);
            tsValue.flag = 15;
          }
        } else if (dataType != DataType.DT_UNKNOWN) {
          throw new AoException(ErrorCode.AO_NOT_IMPLEMENTED, SeverityFlag.ERROR, 0,
              "DataType " + dataType.value() + " not yet implemented");
        }
      }
    }
    return tsValue;
  }

  private Blob parseBlob(AoSession aoSession, String attrName, XMLStreamReader reader)
      throws XMLStreamException, AoException {
    Blob blob = aoSession.createBlob();
    while (!reader.isEndElement() || !reader.getLocalName().equals(attrName)) {
      if (reader.isStartElement() && reader.getLocalName().equals("text")) {
        blob.setHeader(reader.getElementText());
      } else if (reader.isStartElement() && reader.getLocalName().equals("sequence")) {
        blob.append(AtfxParseUtil.parseByteSeq(reader.getElementText()));
      }
      reader.next();
    }
    return blob;
  }

  private byte[][] parseBytestrSeq(String attrName, XMLStreamReader reader)
      throws XMLStreamException, AoException {
    List<byte[]> list = (List) new ArrayList<>();
    while (!reader.isEndElement() || !reader.getLocalName().equals(attrName)) {
      if (reader.isStartElement() && reader.getLocalName().equals("length")) {
        Integer.parseInt(reader.getElementText());
      } else if (reader.isStartElement() && reader.getLocalName().equals("sequence")) {
        list.add(AtfxParseUtil.parseByteSeq(reader.getElementText()));
      }
      reader.next();
    }
    return list.<byte[]>toArray(new byte[0][0]);
  }

  private byte[] parseBytestr(String attrName, XMLStreamReader reader)
      throws XMLStreamException, AoException {
    byte[] bytes = new byte[0];
    try {
      bytes = AtfxParseUtil.parseByteSeq(reader.getElementText());
    } catch (Exception e) {
      while (!reader.isEndElement() || !reader.getLocalName().equals(attrName)) {
        if (!reader.isStartElement() || !reader.getLocalName().equals("length"))
          if (reader.isStartElement() && reader.getLocalName().equals("sequence"))
            bytes = AtfxParseUtil.parseByteSeq(reader.getElementText());
        reader.next();
      }
    }
    return bytes;
  }

  private T_ExternalReference[] parseExtRefs(String attrName, XMLStreamReader reader)
      throws XMLStreamException {
    List<T_ExternalReference> list = new ArrayList<>();
    while (!reader.isEndElement() || !reader.getLocalName().equals(attrName)) {
      if (reader.isStartElement() && reader.getLocalName().equals("external_reference"))
        list.add(parseExtRef(reader));
      reader.next();
    }
    return list.<T_ExternalReference>toArray(new T_ExternalReference[0]);
  }

  private T_ExternalReference parseExtRef(XMLStreamReader reader) throws XMLStreamException {
    T_ExternalReference extRef = new T_ExternalReference("", "", "");
    while (!reader.isEndElement() || !reader.getLocalName().equals("external_reference")) {
      if (reader.isStartElement() && reader.getLocalName().equals("description")) {
        extRef.description = reader.getElementText();
      } else if (reader.isStartElement() && reader.getLocalName().equals("mimetype")) {
        extRef.mimeType = reader.getElementText();
      } else if (reader.isStartElement() && reader.getLocalName().equals("location")) {
        extRef.location = reader.getElementText();
      }
      reader.next();
    }
    return extRef;
  }

  private String[] parseStringSeq(String attrName, XMLStreamReader reader)
      throws XMLStreamException {
    List<String> list = new ArrayList<>();
    while (!reader.isEndElement() || !reader.getLocalName().equals(attrName)) {
      if (reader.isStartElement() && reader.getLocalName().equals("s"))
        list.add(reader.getElementText());
      reader.next();
    }
    return list.<String>toArray(new String[0]);
  }

  public static AtfxInstanceReader getInstance() {
    if (instance == null)
      instance = new AtfxInstanceReader();
    return instance;
  }

  private long parseStartOffset(String offsetValue, String attributeName) throws AoException {
    try {
      if (offsetValue == null || offsetValue.trim().length() <= 0) {
        String reason =
            "empty string not allowed (value of external component with attribute name '"
                + attributeName + "')";
        throw new AoException(ErrorCode.AO_BAD_PARAMETER, SeverityFlag.ERROR, 0, reason);
      }
      return Long.parseLong(offsetValue.trim());
    } catch (NumberFormatException e) {
      double sizeGB = 8.589934592E9D;
      String reason = "The value '" + offsetValue
          + "' of the data file specific external component attribute '" + attributeName
          + "' is not parsable to an long (DT_LONGLONG) value or exceeds the maximal allowed range of '"
          + Long.MAX_VALUE + "' byte (" + sizeGB + " GB)!";
      throw new AoException(ErrorCode.AO_BAD_PARAMETER, SeverityFlag.ERROR, 0, reason);
    }
  }

  private int parseFileLength(String lengthValue, String attributeName) throws AoException {
    try {
      if (lengthValue == null || lengthValue.trim().length() <= 0) {
        String reason =
            "empty string not allowed (value of external component with attribute name '"
                + attributeName + "')";
        throw new AoException(ErrorCode.AO_BAD_PARAMETER, SeverityFlag.ERROR, 0, reason);
      }
      return Integer.parseInt(lengthValue.trim());
    } catch (NumberFormatException e) {
      double sizeGB = 1.9999999990686774D;
      String reason = "The value '" + lengthValue
          + "' of the data file specific external component attribute '" + attributeName
          + "' is not parsable to an integer (DT_LONG) value or exceeds the maximal allowed range of '"
          + 2147483647 + "' byte (" + sizeGB + " GB)!";
      throw new AoException(ErrorCode.AO_BAD_PARAMETER, SeverityFlag.ERROR, 0, reason);
    }
  }
}
