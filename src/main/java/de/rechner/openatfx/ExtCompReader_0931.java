package de.rechner.openatfx;

import de.rechner.openatfx.util.BufferedRandomAccessFile;
import de.rechner.openatfx.util.ODSHelper;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.asam.ods.AoException;
import org.asam.ods.ApplicationRelation;
import org.asam.ods.DataType;
import org.asam.ods.ErrorCode;
import org.asam.ods.NameValue;
import org.asam.ods.SeverityFlag;
import org.asam.ods.TS_Union;
import org.asam.ods.TS_Value;
import org.asam.ods.T_COMPLEX;
import org.asam.ods.T_DCOMPLEX;
import org.asam.ods.T_LONGLONG;

class ExtCompReader_0931 {
  private static final Log LOG = LogFactory.getLog(ExtCompReader_0931.class);

  private static final int BUFFER_SIZE = 32768;
  private static volatile ExtCompReader_0931 instance;

  public TS_Value readValues(AtfxCache atfxCache, long iidLc, DataType targetDataType)
      throws AoException {
    long aidLc =
        ((Long) atfxCache.getAidsByBaseType("aolocalcolumn").iterator().next()).longValue();
    ApplicationRelation relExtComps =
        atfxCache.getApplicationRelationByBaseName(aidLc, "external_component");
    List<Long> iidExtComps = atfxCache.getRelatedInstanceIds(aidLc, iidLc, relExtComps);
    if (iidExtComps.size() > 0)
      Collections.sort(iidExtComps, new ExternalComponentComparator(atfxCache));
    TS_Value tsValue = new TS_Value();
    tsValue.flag = 15;
    tsValue.u = new TS_Union();
    DataType rawDataType = targetDataType;
    Integer attrNo = atfxCache.getAttrNoByBaName(aidLc, "raw_datatype");
    if (attrNo != null) {
      TS_Value valRawDatatype = atfxCache.getInstanceValue(aidLc, attrNo.intValue(), iidLc);
      if (valRawDatatype != null && valRawDatatype.flag == 15) {
        int val = valRawDatatype.u.enumVal();
        if (val == 1) {
          rawDataType = DataType.DS_STRING;
        } else if (val == 2) {
          rawDataType = DataType.DS_SHORT;
        } else if (val == 3) {
          rawDataType = DataType.DS_FLOAT;
        } else if (val == 4) {
          rawDataType = DataType.DS_BOOLEAN;
        } else if (val == 5) {
          rawDataType = DataType.DS_BYTE;
        } else if (val == 6) {
          rawDataType = DataType.DS_LONG;
        } else if (val == 7) {
          rawDataType = DataType.DS_DOUBLE;
        } else if (val == 8) {
          rawDataType = DataType.DS_LONGLONG;
        } else if (val == 10) {
          rawDataType = DataType.DS_DATE;
        } else if (val == 11) {
          rawDataType = DataType.DS_BYTESTR;
        } else if (val == 13) {
          rawDataType = DataType.DS_COMPLEX;
        } else if (val == 14) {
          rawDataType = DataType.DS_DCOMPLEX;
        } else if (val == 28) {
          rawDataType = DataType.DS_EXTERNALREFERENCE;
        } else if (val == 30) {
          rawDataType = DataType.DS_ENUM;
        }
      }
    }
    if (rawDataType == DataType.DS_STRING || rawDataType == DataType.DS_BYTESTR) {
      List<String> list = new ArrayList<>();
      for (Iterator<Long> iterator = iidExtComps.iterator(); iterator.hasNext();) {
        long iidExtComp = ((Long) iterator.next()).longValue();
        list.addAll(readStringValues(atfxCache, iidExtComp));
      }
      tsValue.u.stringSeq(list.<String>toArray(new String[0]));
    } else if (rawDataType == DataType.DS_DATE) {
      List<String> list = new ArrayList<>();
      for (Iterator<Long> iterator = iidExtComps.iterator(); iterator.hasNext();) {
        long iidExtComp = ((Long) iterator.next()).longValue();
        list.addAll(readStringValues(atfxCache, iidExtComp));
      }
      tsValue.u.dateSeq(list.<String>toArray(new String[0]));
    } else {
      List<Number> list = new ArrayList<>();
      for (Iterator<Long> iterator = iidExtComps.iterator(); iterator.hasNext();) {
        long iidExtComp = ((Long) iterator.next()).longValue();
        list.addAll(readNumberValues(atfxCache, iidExtComp, rawDataType));
      }
      if (rawDataType == DataType.DS_BOOLEAN) {
        boolean[] ar = new boolean[list.size()];
        for (int i = 0; i < list.size(); i++)
          ar[i] = (((Number) list.get(i)).byteValue() != 0);
        tsValue.u.booleanSeq(ar);
      } else if (rawDataType == DataType.DS_BYTE) {
        byte[] ar = new byte[list.size()];
        for (int i = 0; i < list.size(); i++)
          ar[i] = ((Number) list.get(i)).byteValue();
        tsValue.u.byteSeq(ar);
      } else if (rawDataType == DataType.DS_SHORT) {
        short[] ar = new short[list.size()];
        for (int i = 0; i < list.size(); i++)
          ar[i] = ((Number) list.get(i)).shortValue();
        tsValue.u.shortSeq(ar);
      } else if (rawDataType == DataType.DS_LONG) {
        int[] ar = new int[list.size()];
        for (int i = 0; i < list.size(); i++)
          ar[i] = ((Number) list.get(i)).intValue();
        tsValue.u.longSeq(ar);
      } else if (rawDataType == DataType.DS_DOUBLE) {
        double[] ar = new double[list.size()];
        for (int i = 0; i < list.size(); i++)
          ar[i] = ((Number) list.get(i)).doubleValue();
        tsValue.u.doubleSeq(ar);
      } else if (rawDataType == DataType.DS_LONGLONG) {
        T_LONGLONG[] ar = new T_LONGLONG[list.size()];
        for (int i = 0; i < list.size(); i++)
          ar[i] = ODSHelper.asODSLongLong(((Number) list.get(i)).longValue());
        tsValue.u.longlongSeq(ar);
      } else if (rawDataType == DataType.DS_FLOAT) {
        float[] ar = new float[list.size()];
        for (int i = 0; i < list.size(); i++)
          ar[i] = ((Number) list.get(i)).floatValue();
        tsValue.u.floatSeq(ar);
      } else if (rawDataType == DataType.DS_COMPLEX) {
        int size = list.size() / 2;
        T_COMPLEX[] ar = new T_COMPLEX[size];
        for (int i = 0; i < size; i++) {
          ar[i] = new T_COMPLEX();
          (ar[i]).r = ((Number) list.get(i * 2)).floatValue();
          (ar[i]).i = ((Number) list.get(i * 2 + 1)).floatValue();
        }
        tsValue.u.complexSeq(ar);
      } else if (rawDataType == DataType.DS_DCOMPLEX) {
        int size = list.size() / 2;
        T_DCOMPLEX[] ar = new T_DCOMPLEX[size];
        for (int i = 0; i < size; i++) {
          ar[i] = new T_DCOMPLEX();
          (ar[i]).r = ((Number) list.get(i * 2)).doubleValue();
          (ar[i]).i = ((Number) list.get(i * 2 + 1)).doubleValue();
        }
        tsValue.u.dcomplexSeq(ar);
      } else {
        throw new AoException(ErrorCode.AO_NOT_IMPLEMENTED, SeverityFlag.ERROR, 0,
            "Reading values from external component not yet supported for datatype: " +

                ODSHelper.dataType2String(targetDataType));
      }
    }
    return tsValue;
  }

  private List<Number> readNumberValues(AtfxCache atfxCache, long iidExtComp, DataType rawDataType)
      throws AoException {
    BufferedRandomAccessFile bufferedRandomAccessFile = null;
    long start = System.currentTimeMillis();
    List<Number> list = new ArrayList<>();
    long aidExtComp =
        ((Long) atfxCache.getAidsByBaseType("aoexternalcomponent").iterator().next()).longValue();
    Integer attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "filename_url");
    String filenameUrl =
        (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.stringVal();
    File fileRoot =
        new File(((NameValue) atfxCache.getContext().get("FILE_ROOT")).value.u.stringVal());

    File extCompFile = getExtComFile(filenameUrl, fileRoot);

    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "value_type");
    int valueType =
        (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.enumVal();
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "component_length");
    int componentLength =
        (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.longVal();
    int startOffset = 0;
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "start_offset");
    TS_Value vStartOffset = atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp);
    if (vStartOffset.u.discriminator() == DataType.DT_LONG) {
      startOffset = vStartOffset.u.longVal();
    } else if (vStartOffset.u.discriminator() == DataType.DT_LONGLONG) {
      startOffset = (int) ODSHelper.asJLong(vStartOffset.u.longlongVal());
    }
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "value_offset");
    int valueOffset =
        (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.longVal();
    short bitCount = 0;
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "ao_bit_count");
    if (attrNo != null)
      bitCount =
          (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.shortVal();
    short bitOffset = 0;
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "ao_bit_offset");
    if (attrNo != null)
      bitOffset =
          (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.shortVal();
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "block_size");
    int blockSize =
        (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.longVal();
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "valuesperblock");
    int valuesperblock =
        (atfxCache.getInstanceValue(aidExtComp, attrNo.intValue(), iidExtComp)).u.longVal();
    RandomAccessFile raf = null;
    try {
      bufferedRandomAccessFile = new BufferedRandomAccessFile(extCompFile, "r", BUFFER_SIZE);
      bufferedRandomAccessFile.seek(startOffset);
      ByteBuffer sourceMbb = ByteBuffer.allocate(blockSize);
      ByteOrder byteOrder = ByteOrder.LITTLE_ENDIAN;
      if (valueType == 7 || valueType == 8 || valueType == 9 || valueType == 10 || valueType == 11
          || valueType == 15 || valueType == 16 || valueType == 17 || valueType == 18
          || valueType == 20 || valueType == 22 || valueType == 24 || valueType == 26
          || valueType == 28 || valueType == 30 || valueType == 32)
        byteOrder = ByteOrder.BIG_ENDIAN;
      sourceMbb.order(byteOrder);
      int i;
      for (i = 0; i < componentLength; i += valuesperblock) {
        byte[] record = new byte[blockSize];
        bufferedRandomAccessFile.read(record, 0, record.length);
        ((Buffer) Buffer.class.cast(sourceMbb)).clear();
        sourceMbb.put(record);
        sourceMbb.position(valueOffset);
        for (int j = 0; j < valuesperblock; j++) {
          if (valueType == 1) {
            list.add(Byte.valueOf(sourceMbb.get()));
          } else if (valueType == 19) {
            list.add(Byte.valueOf(sourceMbb.get()));
          } else if (valueType == 2 || valueType == 7) {
            list.add(Short.valueOf(sourceMbb.getShort()));
          } else if (valueType == 3 || valueType == 8) {
            list.add(Integer.valueOf(sourceMbb.getInt()));
          } else if (valueType == 4 || valueType == 9) {
            list.add(Long.valueOf(sourceMbb.getLong()));
          } else if (valueType == 5 || valueType == 10) {
            list.add(Float.valueOf(sourceMbb.getFloat()));
          } else if (valueType == 6 || valueType == 11) {
            list.add(Double.valueOf(sourceMbb.getDouble()));
          } else if (valueType == 21 || valueType == 22) {
            int val = sourceMbb.getShort() & 0xFFFF;
            list.add(Integer.valueOf(val));
          } else if (valueType == 23 || valueType == 24) {
            long val = sourceMbb.getInt() & 0xFFFFFFFFL;
            list.add(Long.valueOf(val));
          } else if (valueType == 27 || valueType == 28 || valueType == 29 || valueType == 30) {
            int bytesToRead = (bitCount + bitOffset - 1) / 8 + 1;
            byte[] tmp = new byte[bytesToRead];
            sourceMbb.get(tmp);
            list.add(ODSHelper.getBitShiftedIntegerValue(tmp, valueType, bitCount, bitOffset));
          } else {
            throw new AoException(ErrorCode.AO_NOT_IMPLEMENTED, SeverityFlag.ERROR, 0,
                "Unsupported 'value_type': " + ODSHelper.valueType2String(valueType));
          }
        }
      }
      LOG.info("Read " + list.size() + " numeric values from component file '" + filenameUrl
          + "' in " + (System.currentTimeMillis() - start) + "ms [value_type="
          + ODSHelper.valueType2String(valueType) + "]");
      return list;
    } catch (IOException e) {
      LOG.error(e.getMessage(), e);
      throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0, e.getMessage());
    } finally {
      if (bufferedRandomAccessFile != null) {
        try {
          bufferedRandomAccessFile.close();
        } catch (IOException ioe) {
          LOG.error(ioe.getMessage(), ioe);
        }
        bufferedRandomAccessFile = null;
      }
    }
  }

  private File getExtComFile(String filenameUrl, File fileRoot) {
    File testFile = new File(filenameUrl.trim());
    File extCompFile = null;
    if (testFile.isFile() && !testFile.isDirectory()) {
      extCompFile = testFile;
    } else {
      extCompFile = new File(fileRoot, filenameUrl);
    }
    return extCompFile;
  }

  private Collection<String> readStringValues(AtfxCache atfxCache, long iidExtComp)
      throws AoException {
    BufferedRandomAccessFile bufferedRandomAccessFile = null;
    long start = System.currentTimeMillis();
    long aidExtComp =
        ((Long) atfxCache.getAidsByBaseType("aoexternalcomponent").iterator().next()).longValue();
    int attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "filename_url").intValue();
    String filenameUrl = (atfxCache.getInstanceValue(aidExtComp, attrNo, iidExtComp)).u.stringVal();
    File fileRoot =
        new File(((NameValue) atfxCache.getContext().get("FILE_ROOT")).value.u.stringVal());

    File extCompFile = getExtComFile(filenameUrl, fileRoot);

    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "value_type").intValue();
    int valueType = (atfxCache.getInstanceValue(aidExtComp, attrNo, iidExtComp)).u.enumVal();
    if (valueType != 12 && valueType != 25)
      throw new AoException(ErrorCode.AO_NOT_IMPLEMENTED, SeverityFlag.ERROR, 0,
          "Unsupported 'value_type' for data type DT_STRING or DT_DATE: " + valueType);
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "component_length").intValue();
    int componentLength = (atfxCache.getInstanceValue(aidExtComp, attrNo, iidExtComp)).u.longVal();
    long startOffset = 0L;
    attrNo = atfxCache.getAttrNoByBaName(aidExtComp, "start_offset").intValue();
    TS_Value vStartOffset = atfxCache.getInstanceValue(aidExtComp, attrNo, iidExtComp);
    if (vStartOffset.u.discriminator() == DataType.DT_LONG) {
      startOffset = vStartOffset.u.longVal();
    } else if (vStartOffset.u.discriminator() == DataType.DT_LONGLONG) {
      startOffset = ODSHelper.asJLong(vStartOffset.u.longlongVal());
    }
    RandomAccessFile raf = null;
    byte[] backingBuffer = new byte[componentLength];
    List<String> list = new ArrayList<>();
    Charset charset = (valueType == 12) ? StandardCharsets.ISO_8859_1 : StandardCharsets.UTF_8;
    try {
      bufferedRandomAccessFile = new BufferedRandomAccessFile(extCompFile, "r", BUFFER_SIZE);
      bufferedRandomAccessFile.seek(startOffset);
      bufferedRandomAccessFile.read(backingBuffer, 0, backingBuffer.length);
      int startPosition = 0;
      for (int position = 0; position < componentLength; position++) {
        if (backingBuffer[position] == 0) {
          list.add(new String(backingBuffer, startPosition, position - startPosition, charset));
          startPosition = position + 1;
        }
      }
      LOG.info("Read " + list.size() + " string values from component file '" + filenameUrl
          + "' in " + (System.currentTimeMillis() - start) + "ms [value_type="
          + ODSHelper.valueType2String(valueType) + "]");
      return list;
    } catch (IOException e) {
      LOG.error(e.getMessage(), e);
      throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0, e.getMessage());
    } finally {
      try {
        if (bufferedRandomAccessFile != null)
          bufferedRandomAccessFile.close();
        bufferedRandomAccessFile = null;
      } catch (Exception e) {
        LOG.error(e.getMessage(), e);
      }
    }
  }

  public TS_Value readFlags(AtfxCache atfxCache, long iidLc) throws AoException {
    long start = System.currentTimeMillis();
    long aidLc =
        ((Long) atfxCache.getAidsByBaseType("aolocalcolumn").iterator().next()).longValue();
    ApplicationRelation relExtComps =
        atfxCache.getApplicationRelationByBaseName(aidLc, "external_component");
    Collection<Long> iidExtComps = atfxCache.getRelatedInstanceIds(aidLc, iidLc, relExtComps);
    if (iidExtComps.isEmpty())
      return null;
    long aidExtComp =
        ((Long) atfxCache.getAidsByBaseType("aoexternalcomponent").iterator().next()).longValue();
    Iterator<Long> extCompIter = iidExtComps.iterator();
    Integer attrNoComponentLength = atfxCache.getAttrNoByBaName(aidExtComp, "component_length");
    int overallNrOfFlags = 0;
    while (extCompIter.hasNext()) {
      long iidExtComp = ((Long) extCompIter.next()).longValue();
      if (attrNoComponentLength == null)
        return null;
      overallNrOfFlags +=
          (atfxCache.getInstanceValue(aidExtComp, attrNoComponentLength.intValue(), iidExtComp)).u
              .longVal();
    }
    short[] overallFlags = new short[overallNrOfFlags];
    Integer attrNoFlagsFilenameUrl = atfxCache.getAttrNoByBaName(aidExtComp, "flags_filename_url");
    Integer attrNoFlagsStartOffset = atfxCache.getAttrNoByBaName(aidExtComp, "flags_start_offset");
    List<String> flagsFileNames = new ArrayList<>();
    extCompIter = iidExtComps.iterator();
    int flagIndex = 0;
    while (extCompIter.hasNext()) {
      long iidExtComp = ((Long) extCompIter.next()).longValue();
      TS_Value v =
          atfxCache.getInstanceValue(aidExtComp, attrNoFlagsFilenameUrl.intValue(), iidExtComp);
      if (v == null || v.flag != 15 || v.u.stringVal() == null || v.u.stringVal().length() < 1)
        return null;
      String flagsFilenameUrl = v.u.stringVal();
      File fileRoot =
          new File(((NameValue) atfxCache.getContext().get("FILE_ROOT")).value.u.stringVal());

      File flagsFile = getExtComFile(flagsFilenameUrl, fileRoot);

      flagsFileNames.add(flagsFile.getName());
      int flagsStartOffset = 0;
      if (attrNoFlagsStartOffset == null)
        throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0,
            "Application attribute derived from base attribute 'flags_start_offset' not found");
      TS_Value vStartOffset =
          atfxCache.getInstanceValue(aidExtComp, attrNoFlagsStartOffset.intValue(), iidExtComp);
      if (vStartOffset.u.discriminator() == DataType.DT_LONG) {
        flagsStartOffset = vStartOffset.u.longVal();
      } else if (vStartOffset.u.discriminator() == DataType.DT_LONGLONG) {
        flagsStartOffset = (int) ODSHelper.asJLong(vStartOffset.u.longlongVal());
      }
      int componentLength =
          (atfxCache.getInstanceValue(aidExtComp, attrNoComponentLength.intValue(), iidExtComp)).u
              .longVal();
      for (Short flag : readFlagsFromFile(flagsFile, flagsStartOffset, componentLength))
        overallFlags[flagIndex++] = flag.shortValue();
    }
    TS_Value tsValue = new TS_Value();
    tsValue.flag = 15;
    tsValue.u = new TS_Union();
    tsValue.u.shortSeq(overallFlags);
    LOG.info(
        "Read " + overallNrOfFlags + " flags for LocalColumn " + iidLc + " from component file(s) '"
            + (String) flagsFileNames.stream().collect(Collectors.joining(",")) + "' in "
            + (System.currentTimeMillis() - start) + "ms");
    return tsValue;
  }

  private List<Short> readFlagsFromFile(File flagsFile, long flagsStartOffset, int componentLength)
      throws AoException {
    List<Short> flags = new ArrayList<>();
    byte[] backingBuffer = new byte[2];
    try (BufferedRandomAccessFile bufferedRandomAccessFile =
        new BufferedRandomAccessFile(flagsFile, "r", BUFFER_SIZE)) {
      bufferedRandomAccessFile.seek(flagsStartOffset);
      for (int i = 0; i < componentLength; i++) {
        bufferedRandomAccessFile.read(backingBuffer, 0, backingBuffer.length);
        ByteBuffer sourceMbb = ByteBuffer.wrap(backingBuffer);
        sourceMbb.order(ByteOrder.LITTLE_ENDIAN);
        flags.add(Short.valueOf(sourceMbb.getShort()));
      }
    } catch (IOException e) {
      LOG.error(e.getMessage(), e);
      throw new AoException(ErrorCode.AO_NOT_FOUND, SeverityFlag.ERROR, 0, e.getMessage());
    }
    return flags;
  }

  public static ExtCompReader_0931 getInstance() {
    if (instance == null)
      instance = new ExtCompReader_0931();
    return instance;
  }

  private static class ExternalComponentComparator implements Comparator<Long> {
    private final AtfxCache atfxCache;

    public ExternalComponentComparator(AtfxCache atfxCache) {
      this.atfxCache = atfxCache;
    }

    public int compare(Long iidExtComp1, Long iidExtComp2) {
      try {
        Integer ordExtComp1 = Integer.valueOf(0);
        Integer ordExtComp2 = Integer.valueOf(0);
        long aidExtComp =
            ((Long) this.atfxCache.getAidsByBaseType("aoexternalcomponent").iterator().next())
                .longValue();
        Integer attrNo = this.atfxCache.getAttrNoByBaName(aidExtComp, "ordinal_number");
        if (attrNo == null) {
          ExtCompReader_0931.LOG.warn("Application attribute derived from 'ordinal_number' not found!");
          return 0;
        }
        ordExtComp1 = Integer.valueOf((this.atfxCache.getInstanceValue(aidExtComp,
            attrNo.intValue(), iidExtComp1.longValue())).u.longVal());
        ordExtComp2 = Integer.valueOf((this.atfxCache.getInstanceValue(aidExtComp,
            attrNo.intValue(), iidExtComp2.longValue())).u.longVal());
        return ordExtComp1.compareTo(ordExtComp2);
      } catch (AoException e) {
        ExtCompReader_0931.LOG.warn(e.reason, (Throwable) e);
        return 0;
      }
    }
  }
}
